import { IconButton } from '@material-ui/core';
import { Add, Refresh } from '@material-ui/icons';
import React, { useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import useRooms from '../../hooks/useRooms/useRooms';
import {
  getAvailableRooms,
  getParticipatingRooms,
  joinRoom,
} from '../../services/BattleshipeabyService';
import MainLayout from '../MainLayout/MainLayout';
import { NewRoomButton } from './HomeScreenElements';
import RoomsList from './RoomsList/RoomsList';

const HomeScreen = () => {
  const [loadingAvailableRooms, availableRooms, refreshAvailableRooms] = useRooms(
    getAvailableRooms
  );
  const [loadingYourRooms, yourRooms] = useRooms(getParticipatingRooms);
  const history = useHistory();

  const enter = useCallback((id) => history.push(`/rooms/${id}`), [history]);

  const newRoom = useCallback(() => history.push('/rooms/new'), [history]);

  const join = useCallback(
    async (id) => {
      await joinRoom(id);
      enter(id);
    },
    [enter]
  );

  return (
    <MainLayout title="Battleshipeaby">
      <RoomsList
        title="Tus salas"
        loading={loadingYourRooms}
        rooms={yourRooms}
        emptyText="No estas unido a ninguna sala"
        roomItemClick={enter}
        roomItemText="Entrar"
      />
      <RoomsList
        title="Salas disponibles"
        titleButton={
          <IconButton onClick={refreshAvailableRooms}>
            <Refresh />
          </IconButton>
        }
        loading={loadingAvailableRooms}
        rooms={availableRooms}
        emptyText="No hay salas disponibles"
        roomItemClick={join}
        roomItemText="Unirse"
      />
      <NewRoomButton color="primary" aria-label="add" onClick={newRoom}>
        <Add />
      </NewRoomButton>
    </MainLayout>
  );
};

export default HomeScreen;
