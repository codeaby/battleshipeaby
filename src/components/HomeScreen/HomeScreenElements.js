import { Fab, styled } from '@material-ui/core';

export const NewRoomButton = styled(Fab)(({ theme }) => ({
  position: 'fixed',
  bottom: theme.spacing(3),
  right: theme.spacing(3),
}));
