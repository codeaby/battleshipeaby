import { Box, styled, Typography } from '@material-ui/core';
import React from 'react';

export const RoomsListTitle = styled(Box)(({ theme }) => ({
  marginTop: theme.spacing(3),
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
}));

export const EmptyRooms = styled(({ ...others }) => (
  <Typography variant="body1" align="center" {...others} />
))(({ theme }) => ({
  marginTop: theme.spacing(3),
}));
