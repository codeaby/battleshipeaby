import {
  Button,
  List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
  Typography,
} from '@material-ui/core';
import React from 'react';
import Loading from '../../Loading/Loading';
import { EmptyRooms, RoomsListTitle } from './RoomsListElements';

const Rooms = ({ loading, rooms, emptyText, roomItemClick, roomItemText }) => {
  if (loading) return <Loading />;

  if (!rooms || !rooms.length) return <EmptyRooms>{emptyText}</EmptyRooms>;

  return (
    <List>
      {rooms.map((room) => (
        <ListItem key={room.id}>
          <ListItemText primary={room.name} secondary={`De: ${room.player1.name}`} />
          <ListItemSecondaryAction>
            <Button edge="end" onClick={() => roomItemClick(room.id)}>
              {roomItemText}
            </Button>
          </ListItemSecondaryAction>
        </ListItem>
      ))}
    </List>
  );
};

const RoomsList = ({
  title,
  titleButton,
  loading,
  rooms,
  emptyText,
  roomItemClick,
  roomItemText,
}) => {
  return (
    <>
      <RoomsListTitle>
        <Typography variant="h6">{title}</Typography>
        {titleButton}
      </RoomsListTitle>
      <Rooms
        loading={loading}
        rooms={rooms}
        emptyText={emptyText}
        roomItemClick={roomItemClick}
        roomItemText={roomItemText}
      />
    </>
  );
};

export default RoomsList;
