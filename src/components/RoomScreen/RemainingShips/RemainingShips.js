import { Box, Button, Typography } from '@material-ui/core';
import React from 'react';
import { Ship, ShipCell, ShipsContainer } from './RemainingShipsElements';

const ShipsDisplay = ({ remainingShips }) => {
  return (
    <ShipsContainer>
      <Typography variant="h6" align="center" style={{ marginBottom: 8 }}>
        Tus barcos disponibles
      </Typography>
      {remainingShips.map((ship) => (
        <Ship key={ship.id}>
          {[...Array(ship.length).keys()].map((c) => (
            <ShipCell key={`s-${c}`} color={ship.color} />
          ))}
        </Ship>
      ))}
    </ShipsContainer>
  );
};

const PlaceShipments = ({ sendShipPlacement, sent }) => {
  return (
    <Box style={{ marginTop: 16 }}>
      <Button
        variant="contained"
        color="primary"
        fullWidth
        disabled={sent}
        onClick={sendShipPlacement}
      >
        Enviar Posiciones
      </Button>
    </Box>
  );
};

const RemainingShips = ({ remainingShips, sendShipPlacement, sent }) => {
  if (!remainingShips) return null;

  return remainingShips.length ? (
    <ShipsDisplay remainingShips={remainingShips} />
  ) : (
    <PlaceShipments sendShipPlacement={sendShipPlacement} sent={sent} />
  );
};

export default RemainingShips;
