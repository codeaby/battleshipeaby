import { Box, Paper, styled } from '@material-ui/core';

export const ShipsContainer = styled(Paper)({
  margin: '16px 0',
  padding: 8,
});

export const Ship = styled(Box)({
  display: 'flex',
  flexWrap: 'wrap',
  marginBottom: 8,
});

export const ShipCell = styled(Box)({
  width: '10%',
  paddingBottom: '10%',
  border: '1px solid grey',
  backgroundColor: (props) => props.color,
});
