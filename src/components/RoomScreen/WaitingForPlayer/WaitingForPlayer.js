import { Typography } from '@material-ui/core';
import React from 'react';

const WaitingForPlayer = () => {
  return (
    <Typography variant="h6" align="center" style={{ margin: 16 }}>
      Esperando que otro jugador se una a la sala...
    </Typography>
  );
};

export default WaitingForPlayer;
