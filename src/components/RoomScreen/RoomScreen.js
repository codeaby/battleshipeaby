import React, { useCallback, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { ROOM_STATUS } from '../../constants/constants';
import useRoom from '../../hooks/useRoom/useRoom';
import { getBoard } from '../../services/BattleshipeabyService';
import { getShip, initBoard, initGuesses } from '../../utils/roomUtils';
import Loading from '../Loading/Loading';
import MainLayout from '../MainLayout/MainLayout';
import PlayingTurn from './PlayingTurn/PlayingTurn';
import ShipsPlacement from './ShipsPlacement/ShipsPlacement';
import WaitingForPlayer from './WaitingForPlayer/WaitingForPlayer';

const RoomScreen = () => {
  const { id: roomId } = useParams();
  const [room, playerNumber, enemyPlayer] = useRoom(roomId);

  const [board, setBoard] = useState(initBoard());
  const [guesses, setGuesses] = useState(initGuesses());
  const [initLoad, setInitLoad] = useState(false);

  const loadInitBoardAndGuesses = useCallback(async () => {
    const { data } = await getBoard(roomId);
    setBoard(data.board.placements.map((row) => row.row.map((col) => getShip(col))));
    setGuesses(data.board.guesses.map((row) => row.row.map((col) => col)));
    setInitLoad(true);
  }, [roomId]);

  useEffect(() => {
    if (
      room &&
      initLoad === false &&
      (room.status === ROOM_STATUS.PLAYER1_TURN || room.status === ROOM_STATUS.PLAYER2_TURN)
    ) {
      loadInitBoardAndGuesses();
    }
  }, [room, initLoad, loadInitBoardAndGuesses]);

  if (!room) return <Loading />;

  return (
    <MainLayout title="Battleshipeaby" backButtonLink="/">
      {/* TODO: add Room information */}
      {room.status === ROOM_STATUS.WAITING_FOR_PLAYER2 && <WaitingForPlayer />}
      {enemyPlayer && room.status === ROOM_STATUS.WAITING_PLACEMENT && (
        <ShipsPlacement
          roomId={roomId}
          room={room}
          board={board}
          setBoard={setBoard}
          enemyPlayer={enemyPlayer}
        />
      )}
      {(room.status === ROOM_STATUS.PLAYER1_TURN ||
        room.status === ROOM_STATUS.PLAYER2_TURN ||
        room.status === ROOM_STATUS.GAME_OVER) && (
        <PlayingTurn
          roomId={roomId}
          board={board}
          guesses={guesses}
          playerNumber={playerNumber}
          turn={room.status}
          setGuesses={setGuesses}
          gameOver={room.status === ROOM_STATUS.GAME_OVER}
          winner={room.winner}
        />
      )}
    </MainLayout>
  );
};

export default RoomScreen;
