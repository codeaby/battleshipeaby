import { Typography } from '@material-ui/core';
import React, { useState } from 'react';
import { createBoard } from '../../../services/BattleshipeabyService';
import { initShips } from '../../../utils/roomUtils';
import Board from '../Board/Board';
import RemainingShips from '../RemainingShips/RemainingShips';
import ShipsMenu from '../ShipsMenu/ShipsMenu';

const ShipsPlacement = ({ roomId, room, board, setBoard, enemyPlayer }) => {
  const [selectedCell, setSelectedCell] = useState(null);
  const [remainingShips, setRemainingShips] = useState(initShips());
  const [placementsSent, setPlacementsSent] = useState(false); // TODO: Check if already sent from BE

  const handleSelectedCell = (event, cell) =>
    setSelectedCell({ target: event.currentTarget, cell });
  const handleCloseMenu = () => setSelectedCell(null);

  const sendShipPlacement = async () => {
    await createBoard(
      roomId,
      board.map((row) => row.map((col) => col.id))
    );
    setPlacementsSent(true);
  };

  const placeShip = (ship, cell) => {
    const newBoard = board.map((row) => row.map((col) => ({ ...col })));
    for (let i = 0; i < ship.length; i++) {
      if (ship.direction === 'right') {
        newBoard[cell.row][cell.col + i] = { ...ship };
      } else {
        newBoard[cell.row + i][cell.col] = { ...ship };
      }
    }
    setBoard(newBoard);
    setRemainingShips(remainingShips.filter(({ id }) => id !== ship.id));
    handleCloseMenu();
  };

  return (
    <>
      {/* TODO: Pass function for cell click that opens menu for positioning */}
      <Board name="Posiciona tus barcos" board={board} handleSelectedCell={handleSelectedCell} />
      <RemainingShips
        remainingShips={remainingShips}
        sendShipPlacement={sendShipPlacement}
        sent={placementsSent}
      />
      {/* TODO: show this message after placement */}
      <Typography variant="h6" align="center" style={{ margin: 16 }}>
        {room[enemyPlayer].name} todavia esta posicionando sus barcos...
      </Typography>
      <ShipsMenu
        selectedCell={selectedCell}
        handleCloseMenu={handleCloseMenu}
        remainingShips={remainingShips}
        placeShip={placeShip}
      />
    </>
  );
};

export default ShipsPlacement;
