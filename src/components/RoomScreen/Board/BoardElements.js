import { Box, Paper, styled } from '@material-ui/core';

export const BoardContainer = styled(Paper)({
  padding: 8,
});

export const BoardMatrix = styled(Box)({
  display: 'flex',
  flexWrap: 'wrap',
});

export const BoardCell = styled(Box)({
  width: '10%',
  border: '1px solid grey',
  paddingBottom: '10%',
  position: 'relative',
});

export const BoardCellContent = styled(Box)({
  position: 'absolute',
  width: '100%',
  height: '100%',
});
