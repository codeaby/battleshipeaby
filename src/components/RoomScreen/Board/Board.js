import React from 'react';
import { Typography } from '@material-ui/core';
import { BoardCell, BoardCellContent, BoardContainer, BoardMatrix } from './BoardElements';

const isObject = (value) => typeof value === 'object' && value !== null;

const getCellBackgroundColor = (value) => {
  if (isObject(value)) {
    return value.color ? value.color : 'transparent';
  }

  return value === 1 ? 'red' : value === -1 ? 'blue' : 'transparent';
};

const Board = ({ name, board, handleSelectedCell }) => {
  return (
    <BoardContainer>
      <Typography variant="h6" align="center" style={{ marginBottom: 8 }}>
        {name}
      </Typography>
      <BoardMatrix>
        {board.map((row, rowIndex) =>
          row.map((col, colIndex) => (
            <BoardCell
              key={`${rowIndex}-${colIndex}`}
              onClick={(e) =>
                handleSelectedCell && handleSelectedCell(e, { row: rowIndex, col: colIndex })
              }
              style={{ backgroundColor: getCellBackgroundColor(col) }}
            >
              <BoardCellContent></BoardCellContent>
            </BoardCell>
          ))
        )}
      </BoardMatrix>
    </BoardContainer>
  );
};

export default Board;
