import { Button, Dialog, DialogContent, Typography } from '@material-ui/core';
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { ROOM_STATUS } from '../../../constants/constants';
import useAuth from '../../../hooks/useAuth/useAuth';
import { guessCell } from '../../../services/BattleshipeabyService';
import Board from '../Board/Board';

const PlayingTurn = ({
  roomId,
  board,
  guesses,
  playerNumber,
  turn,
  setGuesses,
  gameOver,
  winner,
}) => {
  const myTurn =
    (playerNumber === 'player1' && turn === ROOM_STATUS.PLAYER1_TURN) ||
    (playerNumber === 'player2' && turn === ROOM_STATUS.PLAYER2_TURN);

  const auth = useAuth();
  const [checking, setChecking] = useState(false);
  const history = useHistory();

  const handleSelectedCell = async (event, cell) => {
    if (!gameOver && myTurn) {
      setChecking(true);
      const { data } = await guessCell(roomId, cell.row, cell.col);
      setGuesses(
        guesses.map((row, rowIndex) =>
          row.map((col, colIndex) =>
            rowIndex === cell.row && colIndex === cell.col ? data.hit : col
          )
        )
      );
      setChecking(false);
    }
  };

  return (
    <>
      <Board name="Tus barcos" board={board} />
      <Typography variant="h6" align="center">
        {checking ? 'Confirmando posicion' : myTurn ? 'Tu turno' : 'Esperando a tu oponente'}
      </Typography>
      <Board name="Tu oponente" board={guesses} handleSelectedCell={handleSelectedCell} />
      <Dialog open={gameOver} fullWidth maxWidth="md">
        <DialogContent>
          <Typography variant="h6" align="center" style={{ margin: 16 }}>
            {auth.user.uid === winner ? 'Ganaste!' : 'Perdiste!'}
          </Typography>
          <Button fullWidth variant="contained" color="primary" onClick={() => history.push('/')}>
            Volver al inicio
          </Button>
        </DialogContent>
      </Dialog>
    </>
  );
};

export default PlayingTurn;
