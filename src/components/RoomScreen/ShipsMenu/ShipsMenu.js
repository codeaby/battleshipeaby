import { Menu as MaterialMenu, MenuItem as MaterialMenuItem } from '@material-ui/core';
import { ArrowDownward, ArrowForward, DirectionsBoat } from '@material-ui/icons';
import React from 'react';
import { BOARD } from '../../../constants/constants';

const ShipsMenu = ({ selectedCell, handleCloseMenu, remainingShips, placeShip }) => {
  const menuItems = [];

  if (selectedCell) {
    remainingShips.forEach(({ id, color, length }) => {
      // TODO: Check collision with other ships
      if (selectedCell.cell.col + length <= BOARD.COLS)
        menuItems.push({ id, color, length, direction: 'right' });
      if (selectedCell.cell.row + length <= BOARD.ROWS)
        menuItems.push({ id, color, length, direction: 'bottom' });
    });
  }

  return (
    <MaterialMenu
      id="battleshipeaby-board-menu"
      anchorEl={selectedCell ? selectedCell.target : null}
      keepMounted
      open={!!selectedCell && menuItems.length}
      onClose={handleCloseMenu}
    >
      {selectedCell &&
        menuItems.map((ship) => (
          <MaterialMenuItem
            key={`${ship.id}-${ship.direction}`}
            onClick={() => placeShip(ship, selectedCell.cell)}
          >
            <DirectionsBoat style={{ color: ship.color }}></DirectionsBoat>
            {ship.direction === 'right' ? <ArrowForward /> : <ArrowDownward />}
          </MaterialMenuItem>
        ))}
    </MaterialMenu>
  );
};

export default ShipsMenu;
