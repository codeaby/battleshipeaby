import { Container, styled } from '@material-ui/core';
import React from 'react';
import ApplicationBar from '../ApplicationBar/ApplicationBar';

const MainContainer = styled(Container)(({ theme }) => ({
  marginTop: theme.spacing(3),
  marginBottom: theme.spacing(13),
}));

const MainLayout = ({ children, title, backButtonLink }) => {
  return (
    <>
      <ApplicationBar title={title} backButtonLink={backButtonLink} />
      <MainContainer maxWidth="xs">{children}</MainContainer>
    </>
  );
};

export default MainLayout;
