import { Button, TextField } from '@material-ui/core';
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { createRoom } from '../../services/BattleshipeabyService';
import MainLayout from '../MainLayout/MainLayout';

const NewRoomScreen = () => {
  const [roomName, setRoomName] = useState('');
  const history = useHistory();

  const updateRoomName = (event) => setRoomName(event.target.value);

  const createNewRoom = async () => {
    if (roomName) {
      const { data } = await createRoom(roomName);
      history.push(`/rooms/${data.id}`);
    }
  };

  return (
    <MainLayout title="Nueva Sala" backButtonLink="/">
      <TextField
        required
        label="Nombre"
        variant="outlined"
        fullWidth
        value={roomName}
        onChange={updateRoomName}
      />
      <Button
        variant="contained"
        fullWidth
        color="primary"
        style={{ marginTop: 16 }}
        onClick={createNewRoom}
        disabled={!roomName}
      >
        Crear Sala
      </Button>
    </MainLayout>
  );
};

export default NewRoomScreen;
