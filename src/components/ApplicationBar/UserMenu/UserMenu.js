import { Divider, ListItemAvatar, ListItemText, Menu, MenuItem } from '@material-ui/core';
import React from 'react';
import { MenuAvatar } from '../ApplicationBarElements';

const UserMenu = ({ id, menuAnchorEl, closeUserMenu, userName, userEmail, userAvatar, logout }) => {
  return (
    <Menu
      id={id}
      open={Boolean(menuAnchorEl)}
      anchorEl={menuAnchorEl}
      onClose={closeUserMenu}
      keepMounted
    >
      <MenuItem>
        <ListItemAvatar>
          <MenuAvatar src={userAvatar} />
        </ListItemAvatar>
        <ListItemText primary={userName} secondary={userEmail}></ListItemText>
      </MenuItem>
      <Divider />
      <MenuItem onClick={logout}>
        <ListItemText primary="Salir"></ListItemText>
      </MenuItem>
    </Menu>
  );
};

export default UserMenu;
