import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { ROOMS_COLLECTION } from '../../constants/constants';
import { readDocumentUpdates } from '../../services/Firebase/Firestore';
import { getEnemyPlayerFromRoom, getPlayerFromRoom } from '../../utils/roomUtils';
import useAuth from '../useAuth/useAuth';

const useRoom = (roomId) => {
  const [room, setRoom] = useState();
  const [playerNumber, setPlayerNumber] = useState();
  const [enemyPlayer, setEnemyPlayer] = useState();
  const { user } = useAuth();
  const history = useHistory();

  useEffect(() => {
    readDocumentUpdates(
      ROOMS_COLLECTION,
      roomId,
      (doc) => {
        setRoom(doc.data());
        setPlayerNumber(getPlayerFromRoom(doc.data(), user.uid));
        setEnemyPlayer(getEnemyPlayerFromRoom(doc.data(), user.uid));
      },
      (e) => {
        console.error(e);
        history.push('/');
      }
    );
  }, [roomId, history, user.uid]);

  return [room, playerNumber, enemyPlayer];
};

export default useRoom;
