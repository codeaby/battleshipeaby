import { useCallback, useState } from 'react';

/**
 * Hook that helps handle Material-UI Menu's open and close functions.
 *
 * @param {function} openCallback will be called with the rest of the parameters
 * @param {function} closeCallback will be called with the rest of the parameters
 * @returns {array} [menuAnchorEl, openMenu, closeMenu]
 */
const useMenu = (openCallback, closeCallback) => {
  const [menuAnchorEl, setMenuAnchorEl] = useState(null);

  const openMenu = useCallback(
    (event, ...other) => {
      setMenuAnchorEl(event.currentTarget);
      openCallback && openCallback(...other);
    },
    [openCallback]
  );

  const closeMenu = useCallback(
    (...other) => {
      setMenuAnchorEl(null);
      closeCallback && closeCallback(...other);
    },
    [closeCallback]
  );

  return [menuAnchorEl, openMenu, closeMenu];
};

export default useMenu;
