import { useCallback, useEffect, useState } from 'react';

/**
 * Hook for rooms loading.
 * @param {function} getRooms endpoint to load rooms
 * @return [loading, rooms, refreshRooms]
 */
const useRooms = (getRooms) => {
  const [loading, setLoading] = useState(true);
  const [rooms, setRooms] = useState([]);

  const loadRooms = useCallback(async () => {
    setLoading(true);
    const { data } = await getRooms();
    setRooms(data.rooms);
    setLoading(false);
  }, [getRooms]);

  useEffect(() => {
    loadRooms();
  }, [loadRooms]);

  return [loading, rooms, loadRooms];
};

export default useRooms;
