import axios from 'axios';
import { getAuthorizationHeader } from './Firebase/Authentication';

const BASE_URL = 'https://us-central1-codeaby-486b9.cloudfunctions.net';

const getAxios = async () =>
  axios.create({
    baseURL: `${BASE_URL}/battleshipeaby`,
    headers: await getAuthorizationHeader(),
  });

export const getAvailableRooms = async () => (await getAxios()).get(`/rooms/available`);

export const getParticipatingRooms = async () => (await getAxios()).get(`/rooms/participating`);

export const createRoom = async (name) => (await getAxios()).post(`/rooms`, { name });

export const joinRoom = async (id) => (await getAxios()).post(`/rooms/${id}/join`);

export const createBoard = async (roomId, placements) =>
  (await getAxios()).post(`/rooms/${roomId}/boards`, { placements });

export const guessCell = async (roomId, row, col) =>
  (await getAxios()).post(`/rooms/${roomId}/guesses`, { guess: { row, col } });

export const getBoard = async (roomId) => (await getAxios()).get(`/rooms/${roomId}/board`);
