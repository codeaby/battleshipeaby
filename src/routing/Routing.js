import React from 'react';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import useAuth from '../hooks/useAuth/useAuth';
import PrivateRoute from './PrivateRoute/PrivateRoute';
import LoginScreen from '../components/LoginScreen/LoginScreen';
import RoomScreen from '../components/RoomScreen/RoomScreen';
import HomeScreen from '../components/HomeScreen/HomeScreen';
import Loading from '../components/Loading/Loading';
import NewRoomScreen from '../components/NewRoomScreen/NewRoomScreen';

const Routing = () => {
  const { loaded } = useAuth();

  if (!loaded) return <Loading fullScreen />;

  return (
    <BrowserRouter basename="/battleshipeaby">
      <Switch>
        <Route path="/login">
          <LoginScreen />
        </Route>
        <PrivateRoute path="/rooms/new">
          <NewRoomScreen />
        </PrivateRoute>
        <PrivateRoute path="/rooms/:id">
          <RoomScreen />
        </PrivateRoute>
        <PrivateRoute path="/" exact>
          <HomeScreen />
        </PrivateRoute>
        <Redirect to="/" />
      </Switch>
    </BrowserRouter>
  );
};

export default Routing;
