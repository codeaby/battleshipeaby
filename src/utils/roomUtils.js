import { BOARD } from '../constants/constants';

export const getPlayerFromRoom = (room, uid) => (room.player1.uid === uid ? 'player1' : 'player2');
export const getEnemyPlayerFromRoom = (room, uid) =>
  room.player1.uid === uid ? 'player2' : 'player1';

export const initBoard = () =>
  [...Array(BOARD.ROWS).keys()].map((row) => Array(BOARD.COLS).fill({ id: 0 }));

export const initGuesses = () =>
  [...Array(BOARD.ROWS).keys()].map((row) => Array(BOARD.COLS).fill(0));

const SHIPS = [
  {
    id: 1,
    color: 'yellow',
    length: 2,
  },
  {
    id: 2,
    color: 'red',
    length: 3,
  },
  {
    id: 3,
    color: 'green',
    length: 5,
  },
];

export const getShip = (id) => ({ ...SHIPS.find((ship) => ship.id === id) });

export const initShips = () => [...SHIPS];
