import { createMuiTheme, CssBaseline, ThemeProvider } from '@material-ui/core';
import React from 'react';
import AuthenticationProvider from '../AuthenticationProvider/AuthenticationProvider';

const MainProviders = ({ children }) => {
  const theme = createMuiTheme({
    palette: {
      type: 'dark',
    },
  });

  return (
    <AuthenticationProvider>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        {children}
      </ThemeProvider>
    </AuthenticationProvider>
  );
};

export default MainProviders;
