import React from 'react';
import MainProviders from './MainProviders/MainProviders';

const Providers = ({ children }) => {
  return <MainProviders>{children}</MainProviders>;
};

export default Providers;
